exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['src/tests/e2e/*.js'],
    capabilities: {
        'browserName': 'chrome'
    },
    directConnect: true
};
