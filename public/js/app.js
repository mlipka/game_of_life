'use strict';

/*
 * Copyright Efigence.com
 * @author - undefined undefined | Efigence.com
 *
 */

(function () {

    'use strict';

    /*global angular*/

    var app = angular.module('gameOfLife', []);

    app.controller('gameController', ['$scope', '$interval', function ($scope, $interval) {
        $scope.setWidth = 40;
        $scope.setHeight = 20;
        $scope.playing = false;
        $scope.intervalLength = 50;

        $scope.init = function () {
            $scope.width = $scope.setWidth;
            $scope.height = $scope.setHeight;
            if ($scope.interval) {
                $interval.cancel($scope.interval);
            }
            $scope.playing = false;
            var k, i, row, board;
            board = [];
            for (k = 0; k < $scope.height; k = k + 1) {
                row = [];
                for (i = 0; i < $scope.width; i = i + 1) {
                    row.push({ value: false, row: k, col: i });
                }
                board.push(row);
            }
            $scope.playboard = board;
            $scope.step = 0;
        };

        $scope.stopPlay = function () {
            if ($scope.playing) {
                $interval.cancel($scope.interval);
                $scope.playing = false;
            } else {
                $scope.interval = $interval(function () {
                    $scope.nextStep();
                }, $scope.intervalLength);
                $scope.playing = true;
            }
        };

        $scope.nextStep = function () {
            $scope.advanceBoard();
            $scope.step += 1;
        };

        $scope.toggle = function (row, index) {
            row[index].value = !row[index].value;
        };

        $scope.neighbors = function (row_number, col_number) {
            var k, i, neighbors;
            if (row_number === 0) {
                row_number = $scope.height;
            }
            if (col_number === 0) {
                col_number = $scope.width;
            }
            neighbors = [];
            for (k = row_number - 1; k <= row_number + 1; k += 1) {
                for (i = col_number - 1; i <= col_number + 1; i += 1) {
                    if (k !== row_number || i !== col_number) {
                        neighbors.push($scope.playboard[k % $scope.height][i % $scope.width]);
                    }
                }
            }
            k = 0;
            for (i = 0; i < neighbors.length; i += 1) {
                if (neighbors[i].value) {
                    k += 1;
                }
            }
            return k;
        };

        $scope.advanceBoard = function () {
            var i, k, changed, cell, next;
            changed = [];
            for (k = 0; k < $scope.height; k += 1) {
                for (i = 0; i < $scope.width; i += 1) {
                    cell = $scope.playboard[k][i];
                    next = $scope.advanceCell(cell);
                    if (cell.value !== next) {
                        changed.push({ row: cell.row, col: cell.col, value: next });
                    }
                }
            }
            for (k = 0; k < changed.length; k += 1) {
                cell = changed[k];
                $scope.playboard[cell.row][cell.col] = cell;
            }
        };

        $scope.advanceCell = function (cell) {
            var neighbors, current_cell, row_number, col_number;
            row_number = cell.row;
            col_number = cell.col;
            neighbors = $scope.neighbors(row_number, col_number);
            current_cell = $scope.playboard[row_number][col_number];
            if (current_cell.value) {
                return neighbors === 2 || neighbors === 3;
            }
            return neighbors === 3;
        };

        $scope.init();
    }]);
})();