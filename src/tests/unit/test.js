//tests

describe('gameController', function () {
    beforeEach(module('gameOfLife'));

    var $controller, $scope, controller;

    beforeEach(inject(function(_$controller_){
        $controller = _$controller_;
        $scope = {};
        controller = $controller('gameController', { $scope: $scope });
        $scope.init();
    }));

    it('sets initial values', function () {
        expect($scope.setWidth).toEqual(40);
        expect($scope.setHeight).toEqual(20);
    });

    it('initializes playboard', function () {
        expect($scope.playing).toEqual(false);
        expect($scope.step).toEqual(0);
        expect($scope.playboard.length).toEqual($scope.height);
        expect($scope.playboard[0].length).toEqual($scope.width);
    });

    it('starts and stops the game', function () {
        $scope.stopPlay();
        expect($scope.playing).toEqual(true);
        expect($scope.interval.$$state.status).toEqual(0);
        $scope.stopPlay();
        expect($scope.playing).toEqual(false);
        expect($scope.interval.$$state.value).toEqual('canceled');
    });

    it('toggles fields on playboard', function () {
        var row = $scope.playboard[5];
        var col = 10;
        $scope.toggle(row, col);
        expect(row[col].value).toBe(true);
    });

    describe('advancing playboard', function () {
        var row, col;
        beforeEach(function () {
            row = 5;
            col = 10;
            // make some neighbors
            $scope.toggle($scope.playboard[row + 1], col);
            $scope.toggle($scope.playboard[row - 1], col - 1);
            $scope.toggle($scope.playboard[row - 1], col + 1);
        });

        it('returns neighbors properly', function () {
            var neighbors = $scope.neighbors(row, col);
            expect(neighbors).toEqual(3);
        });

        it('advances board properly', function () {
            $scope.advanceBoard();
            expect($scope.playboard[row][col].value).toBe(true);
            expect($scope.neighbors(row, col)).toEqual(0);
        });
    });
});
