function clickField(row, col) {
    element.all(by.repeater('row in playboard').row(row))
        .all(by.repeater('cell in row').row(col)).click();
}

describe('Game of life game', function () {
    beforeEach(function() {
        browser.get('/');
    });

    it('should display board with proper dimensions', function () {
        var table = element(by.css('.table-bordered'));
        expect(table.isPresent()).toBe(true);
        var rows_count = element(by.model('setHeight')).getAttribute('value');
        var rows = element.all(by.repeater('row in playboard'));
        rows_count.then(function(value) {
            expect(rows.count()).toEqual(parseInt(value));
        });
        var columns_count = element(by.model('setWidth')).getAttribute('value');
        var columns = element.all(by.repeater('row in playboard').row(0))
            .all(by.repeater('cell in row'));
        columns_count.then(function(value) {
            expect(columns.count()).toEqual(parseInt(value));
        });
    });

    it('should play game of life properly', function () {
        clickField(0, 1);
        clickField(1, 1);
        clickField(2, 1);
        clickField(3, 1);
        element(by.css('[ng-click="stopPlay()"]')).click();
        browser.wait(function () {
            return element(by.binding('step')).getText().then(function (text) {
                return text == 'Step: 2';
            });
        });
        element(by.css('[ng-click="stopPlay()"]')).click();
        expect(element.all(by.css('.black')).count()).toEqual(6);
    });

    it('should resize playboard', function () {
        var height = '40';
        var width = '80';
        element(by.model('setHeight')).clear().sendKeys(height);
        element(by.model('setWidth')).clear().sendKeys(width);
        element(by.css('[ng-click="init()"]')).click();
        expect(element.all(by.repeater('row in playboard')).count())
            .toEqual(parseInt(height));
        expect(element.all(by.repeater('row in playboard').row(0))
            .all(by.repeater('cell in row')).count()).toEqual(parseInt(width));
    });

    it('should go to next step', function () {
        clickField(0, 0);
        element(by.css('[ng-click="nextStep()"]')).click();
        expect(element.all(by.css('.black')).count()).toEqual(0);
        element(by.binding('step')).getText().then(function (text) {
            expect(text).toEqual('Step: 1');
        });
    });
});
